import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Home from './screens/Home';
import RestaurantDetail from './screens/RestaurantDetail';
import { Provider as ReduxProvider } from 'react-redux';
import configureStore from './redux/store';
import OrderCompleted from './screens/OrderCompleted';
import Login from './screens/Login';
import SignUp from './screens/SignUp';
import PhotoCloud from './screens/PhotoCloud';
import Maps from './screens/Maps';

const store = configureStore();
const Stack = createNativeStackNavigator();
const screenOptions = {
    headerShown: false,
};

export const SignedInStack = () => (
    <ReduxProvider store={store}>
        <NavigationContainer>
            <Stack.Navigator
                initialRouteName='Login'
                screenOptions={screenOptions}
            >
                <Stack.Screen name='Home' component={Home} />
                <Stack.Screen
                    name='RestaurantDetail'
                    component={RestaurantDetail}
                />
                <Stack.Screen
                    name='OrderCompleted'
                    component={OrderCompleted}
                />
                <Stack.Screen name='Login' component={Login} />
                <Stack.Screen name='SignUp' component={SignUp} />
                <Stack.Screen name='PhotoCloud' component={PhotoCloud} />
                <Stack.Screen name='Maps' component={Maps} />
            </Stack.Navigator>
        </NavigationContainer>
    </ReduxProvider>
);

export const SignedOutStack = () => {
    <NavigationContainer>
        <Stack.Navigator
            initialRouteName='Login'
            screenOptions={screenOptions}
        ></Stack.Navigator>
    </NavigationContainer>;
};
