import React, { useEffect, useState } from 'react';
import { SignedInStack, SignedOutStack } from './navigation';
import { firebase } from './firebase';
import Login from './screens/Login';

const AuthNavigation = () => {
    const [currentUser, setCurrentUser] = useState(null);

    const userHandler = (user) =>
        user ? setCurrentUser(user) : setCurrentUser(null);

    useEffect(
        () => firebase.auth().onAuthStateChanged((user) => userHandler(user)),
        []
    );

    console.log(currentUser);
    return <>{currentUser ? <SignedInStack /> : <Login />}</>;
};

export default AuthNavigation;
