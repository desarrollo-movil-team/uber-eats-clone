import React, { useState } from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { firebase } from '../../firebase';

// const handleSignOut = async ({ navigation }) => {
//     try {
//         await firebase.auth().signOut();
//         console.log('Signed Out Succesfully!');
//         navigation.push('SignedOut');
//     } catch (error) {
//         console.log(error);
//     }
// };

const HeaderTabs = (props) => {
    return (
        <View
            style={{
                flexDirection: 'row',
                alignSelf: 'center',
                // marginLeft: 85,
            }}
        >
            <HeaderButton
                text='Delivery'
                btnColor='black'
                textColor='white'
                activeTab={props.activeTab}
                setActiveTab={props.setActiveTab}
            />

            <HeaderButton
                text='Pickup'
                btnColor='white'
                textColor='black'
                activeTab={props.activeTab}
                setActiveTab={props.setActiveTab}
            />
            {/* 
            <TouchableOpacity onPress={handleSignOut}>
                <Image
                    style={{
                        width: 25,
                        height: 25,
                        marginLeft: 60,
                    }}
                    source={require('../../assets/icons/sign_out.png')}
                />
            </TouchableOpacity> */}
        </View>
    );
};

const HeaderButton = (props) => (
    <TouchableOpacity
        style={{
            backgroundColor: props.activeTab === props.text ? 'black' : 'white',
            paddingVertical: 6,
            paddingHorizontal: 16,
            borderRadius: 30,
        }}
        onPress={() => props.setActiveTab(props.text)}
    >
        <Text
            style={{
                color: props.activeTab === props.text ? 'white' : 'black',
                fontSize: 15,
                fontWeight: '900',
            }}
        >
            {props.text}
        </Text>
    </TouchableOpacity>
);

export default HeaderTabs;
