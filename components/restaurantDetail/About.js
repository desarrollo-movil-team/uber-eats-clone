import React from 'react';
import { View, Text, Image } from 'react-native';

const yelpRestaurantInfo = {
    name: 'Nigth Time Restaurant Ressort',
    image: 'https://static.standard.co.uk/s3fs-public/thumbnails/image/2018/08/20/16/duck-waffle-night-time-view.jpg?width=1200',
    price: '$$',
    reviews: '1550',
    rating: 5,
    categories: [
        { title: 'Suit' },
        { title: 'Ressort and Dinner' },
        { title: 'Restaurant' },
    ],
};

const About = (props) => {
    const { name, image, price, reviews, rating, categories } =
        props.route.params;

    const formattedCategories = categories.map((cat) => cat.title).join(' • ');

    const descriptionDynamic = `${formattedCategories} ${
        price ? ' • ' + price : ''
    } • 💳 • ${rating}⭐ (${reviews}+)`;

    return (
        <View>
            <RestaurantImage image={image} />
            <RestaurantName name={name} />
            <RestaurantDescription description={descriptionDynamic} />
        </View>
    );
};

const RestaurantImage = (props) => (
    <Image
        source={{ uri: props.image }}
        style={{ width: '100%', height: 180 }}
    />
);

const RestaurantName = (props) => (
    <Text
        style={{
            fontSize: 28,
            fontWeight: '600',
            marginTop: 10,
            marginHorizontal: 15,
        }}
    >
        {props.name}
    </Text>
);
const RestaurantDescription = (props) => (
    <Text
        style={{
            fontSize: 15,
            fontWeight: '400',
            marginTop: 10,
            marginHorizontal: 15,
        }}
    >
        {props.description}
    </Text>
);
export default About;
