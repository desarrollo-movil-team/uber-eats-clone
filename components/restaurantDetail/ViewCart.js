import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { View, Text, TouchableOpacity, Modal, StyleSheet } from 'react-native';
import LottieView from 'lottie-react-native';
import OrderItem from './OrderItem';
import { firebase, db } from '../../firebase';

const ViewCart = ({ navigation }) => {
    const [modalVisible, setModalVisible] = useState(false);
    const [loading, setLoading] = useState(false);

    const { items, restaurantName } = useSelector(
        (state) => state.cartReducer.selectedItems
    );

    const total = items
        .map((item) => Number(item.price.replace('$', '')))
        .reduce((previous, current) => previous + current, 0);

    // const totalUSD = total.toLocaleString('en-US', {
    //     style: 'currency',
    //     currency: 'USD',
    // });

    const totalUSD = total.toPrecision(4);
    console.log(totalUSD);

    const addOrderToFirestore = () => {
        setLoading(true);
        db.collection('orders')
            .add({
                items: items,
                restaurantName: restaurantName,
                createdAt: firebase.firestore.FieldValue.serverTimestamp(),
            })
            .then(() => {
                setTimeout(() => {
                    setLoading(false);
                    navigation.navigate('OrderCompleted');
                }, 3000);
            });
    };

    const styles = StyleSheet.create({
        modalContainer: {
            flex: 1,
            justifyContent: 'flex-end',
            backgroundColor: 'rgba(0,0,0,0.7)',
        },
        modalCheckoutContainer: {
            backgroundColor: 'white',
            padding: 16,
            height: 500,
            borderWidth: 1,
        },
        restaurantName: {
            textAlign: 'center',
            fontWeight: '600',
            justifyContent: 'space-between',
            marginTop: 15,
        },
        subtotalContainer: {
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginTop: 15,
        },
        subTotalText: {
            textAlign: 'left',
            fontWeight: '600',
            fontSize: 15,
            marginBottom: 10,
        },
    });

    const checkoutModalContent = () => {
        return (
            <>
                <View style={styles.modalContainer}>
                    <View style={styles.modalCheckoutContainer}>
                        <Text style={styles.restaurantName}>
                            {restaurantName}
                        </Text>
                        {items.map((item, index) => (
                            <OrderItem item={item} key={index} />
                        ))}
                        <View style={styles.subtotalContainer}>
                            <Text style={styles.subTotalText}>Subtotal</Text>
                            <Text>${totalUSD} USD</Text>
                        </View>
                        <View
                            style={{
                                flexDirection: 'row',
                                justifyContent: 'center',
                                // right: -10,
                            }}
                        >
                            <TouchableOpacity
                                style={{
                                    marginTop: 20,
                                    backgroundColor: 'black',
                                    alignItems: 'center',
                                    padding: 17,
                                    borderRadius: 30,
                                    width: 300,
                                    position: 'relative',
                                }}
                                onPress={() => {
                                    addOrderToFirestore();
                                    setModalVisible(false);
                                }}
                            >
                                <Text
                                    style={{
                                        color: 'white',
                                        fontSize: 20,
                                        // alignSelf: 'flex-start',
                                        marginLeft: -35,
                                    }}
                                >
                                    Pay
                                </Text>
                                <Text
                                    style={{
                                        color: 'white',
                                        fontSize: 16,
                                        backgroundColor: 'green',
                                        borderRadius: 100,
                                        padding: 6,
                                        top: 15,
                                        position: 'absolute',
                                        right: 25,
                                    }}
                                >
                                    ${total ? totalUSD : ''} USD
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </>
        );
    };

    return (
        <>
            <Modal
                animationType='slide'
                visible={modalVisible}
                transparent={true}
                onRequestClose={() => setModalVisible(false)}
            >
                {checkoutModalContent()}
            </Modal>
            {total ? (
                <View
                    style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center',
                        flexDirection: 'row',
                        position: 'absolute',
                        bottom: 15,
                        zIndex: 999,
                    }}
                >
                    <View
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'center',
                            width: '100%',
                        }}
                    >
                        <TouchableOpacity
                            style={{
                                flexDirection: 'row',
                                justifyContent: 'flex-end',
                                position: 'relative',
                                width: 300,
                                padding: 10,
                                marginTop: 20,
                                borderRadius: 30,
                                backgroundColor: 'black',
                            }}
                            onPress={() => setModalVisible(true)}
                        >
                            <Text
                                style={{
                                    color: 'white',
                                    fontSize: 20,
                                    marginRight: 25,
                                }}
                            >
                                {loading ? 'processing...' : 'View Cart'}
                            </Text>
                            <Text
                                style={{
                                    color: 'white',
                                    fontSize: 16,
                                    marginRight: 15,
                                    borderRadius: 100,
                                    padding: 6,
                                    paddingHorizontal: 10,
                                    backgroundColor: 'green',
                                    justifyContent: 'center',
                                }}
                            >
                                ${totalUSD} USD
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            ) : (
                <></>
            )}
            {loading ? (
                <View
                    style={{
                        backgroundColor: 'black',
                        position: 'absolute',
                        opacity: 0.6,
                        justifyContent: 'center',
                        alignItems: 'center',
                        width: '100%',
                        height: '100%',
                        padding: 100,
                    }}
                >
                    <LottieView
                        style={{ height: 200 }}
                        source={require('../../assets/animations/scanner.json')}
                        autoPlay
                        speed={3}
                        loop={true}
                    />
                </View>
            ) : (
                <></>
            )}
        </>
    );
};

export default ViewCart;
